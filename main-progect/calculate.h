#ifndef calc
#define calc
#include "marafon.h"
#include <string>
#include <cmath>

using namespace std;

string Calculate(Data* data, int n)
{
	string Max = data[0].finish;
	for (int i = 0; i < n; i++)
		Max = min(data[i].finish, Max);
	return Max;
}


#endif
