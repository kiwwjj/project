#ifndef mywork
#define mywork
#include "marafon.h"
#include "mydata.h"
#include <array>

void showSpartak(Data* data, int n)
{
    for (int i = 0; i < n; i++)
    {
        if (data[i].club == "sportak") {
            mydata(data[i]);
        }
    }
}
void compareData(Data* data, int n) {

    for (int i = 0; i < n; i++)
    {
        if (data[i].finish < "2:50:00")
           mydata(data[i]);
    }
}
void quickSort(Data* array, int low, int high, bool comp(Data, Data))
{
    int i = low;
    int j = high;
    Data pivot = array[(i + j) / 2];  //������� �������

    while (i <= j)
    {
        while (comp(array[i], pivot))   //��������� �� �������� ��������
            i++;
        while (!comp(array[j], pivot))
            j--;
        if (i <= j)
        {
            Data temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i++;
            j--;
        }
    }
    if (j > low)
        quickSort(array, low, j, comp);   // �� ������� �����, �������� �� ������� ���������
    if (i < high)
        quickSort(array, i, high, comp);   //
}
void bubbleSort(Data* arr, int a, int n, bool comp(Data, Data))
{
    int i, j;
    Data temp;   // ���������� ��� ������ ��������� �������
    for (i = a; i < n; i++)
    {
        for (j = a; j < (n - i - 1); j++)
        {
            if (comp(arr[i], arr[j]))
            {
                //std::cout << arr[i].finish << ' ' << arr[j].finish << '\n';
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}
bool compByTime(Data a, Data b)
{
    return a.finish < b.finish;
}
bool compClub(Data a, Data b)
{

    if (a.club < b.club)
    {
        return 1;
    }
    else {
        if (a.club == b.club)
        {
            return a.Second_name < b.Second_name;
        }
        return 0;
    }
}
/*/auto comps = std:: to_array({ compByTime, compClub });
auto sorts = std::to_array({ bubbleSort, quickSort });//*/

#endif